#!/usr/bin/env python3
from random import randrange

### List Hell

size_list = ['Gigantesque (plus de 30 étages)', 'Grand (entre 10 et 15 étages)', 'Moyen (5 et 10 étages)', 'Petit (moins de 5 étages)', 'Un seul étage']
no_build_list = ['un jardin privé', 'un parc', 'une décharge', 'un terrain vague', 'des débris', 'un bidonville', 'un parking', 'un chantier ']
security_list = ['Extrême', 'Haut', 'Haut', 'Moyen', 'Moyen', 'Moyen', 'Bas', 'Bas', 'Nul']
owners_list = ['une Corporation', 'un marchand de sommeil', 'une association locale', 'la municipalité locale', 'un gang local',
'ceux qui y résident', "n'importe qui qui souhaiterait le revendiquer"]
corps_list = ['Arasaka', 'Biotechnica', 'Continental Brands', 'Danger Girl', 'Militech International', 
'Network 54', 'Petrochem', 'Rocklin Augmentics', 'SovOil', 'Trauma Team', 'Ziggurat', 'Zhirafa Technical Manufacturing', 'Corpo mineure']
status_list = ['surpeuplé', 'délabré', 'flambant neuf', 'désert', 'décoré de façon artistique', 'particulièrement laid',
'une couverture pour des activités bien plus sinistre']

class Thing_next_door():
    def __init__(self):
        self.lapin = 'lapin'
        self.size = ''
        self.is_building = randrange(6)
        if (not (self.is_building)):
            self.type = no_build_list[randrange(len(no_build_list))]
        else:
            self.size = size_list[randrange(len(size_list))]
            self.security = security_list[randrange(len(security_list))]
            self.owner = owners_list[randrange(len(owners_list))]
            if (self.owner == 'une Corporation'):
                self.owner = self.owner + ' (' + corps_list[randrange(len(corps_list))] + ')'
            self.status = status_list[randrange(len(status_list))]

### Print

test = Thing_next_door()
print('')
if (test.is_building):
    print("Il y a un building.")
    print('Il appartient à', test.owner, "et a la particularité d'être", test.status + '.')
    print("Taille:", test.size)
    print('Niveau de sécurité:', test.security)
else:
    print("Pas de building ici. Par contre vous y trouvez", test.type + '.')
